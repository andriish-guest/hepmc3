/**

  @defgroup python Python Bindings

@section pythonintro Introduction

  HepMC includes Python bindings codes suitable for compilation of python modules for Python2.6+ and Python3.

The binding codes were  generated automatically using the binder utility version 0.1 created by Sergey Lyskov, Johns Hopkins University.
See 
- https://cppbinder.readthedocs.io/en/latest/ for details.

The binding codes use the pybinder11 library by Wenzel Jakob, EPFL's School of Computer and Communication Sciences.
See 
- https://pybind11.readthedocs.io/en/master/ 
- Wenzel Jakob and Jason Rhinelander and Dean Moldovan, "pybind11 -- Seamless operability between C++11 and Python", 2017,  https://github.com/pybind/pybind11



So far the bindings are available for all classes in HepMC3 and LHEF namespaces but some in Search engine.
For usage examples please look into the tests.

@section installationnotes Installation notes 

To turn on the compilation of bindings use -DHEPMC3_ENABLE_PYTHON = ON. 
By default the python modules will be generated for python2 and python3 if these are found in the system.
To enable/disable building of python modules please edit CmakeLists.txt directly.
In case the test suite is enabled, tests of python bindings wiath all the enabled versions will run as well.

Despite not recommended, it should be possible to compile the python bindings using the installed version of HepMC3.
To do this, copy the python directory outside of source tree,  uncomment #project(pyHepMC3 CXX) in  python/CMakeLists.txt and 
run cmake inside python directory with -DUSE_INSTALLED_HEPMC3=ON  option. 

@section developernotes Developer notes 
- In the automatically generated codes it was assumed that std::ostream 
will be mapped onto io.stringIO() and simailar objests.
- The constructors of Readers/Writers with ifstreams/ostreams were not binded.

**/
